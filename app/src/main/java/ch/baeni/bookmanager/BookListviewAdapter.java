package ch.baeni.bookmanager;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ch.baeni.bookmanager.vo.Book;

/**
 * Created by Benjamin on 01.07.2017.
 */

class BookListviewAdapter extends ArrayAdapter<Book> {
    private final Context context;

    public BookListviewAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Book> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.simple_book_overview_list, null);
            convertView.setClickable(true);
            TextView textView = (TextView) convertView.findViewById(R.id.listviewText);
            Book book = getItem(position);
            textView.setText(book.getTitle());
        }
        return convertView;
    }
}
