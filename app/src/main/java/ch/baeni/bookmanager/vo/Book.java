package ch.baeni.bookmanager.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 30.06.2017.
 */

public class Book {
    private final long id;
    private String title;
    private String description;
    private int publishedYear;
    private String isbn;
    private final List<Author> authors = new ArrayList<>();

    public Book(Long bookId, String title, String description, int publishedYear, String isbn) {
        id = bookId;
        this.title = title;
        this.description = description;
        this.publishedYear = publishedYear;
        this.isbn = isbn;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public String getIsbn() {
        return isbn;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getDescription() {
        return description;
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    public String getAuthorsAsString() {
        StringBuilder sb = new StringBuilder();
        for (Author a : getAuthors()){
            sb.append(a.getLastName());
            sb.append(" ");
            sb.append(a.getFirstName());
            sb.append(", ");
        }
        return sb.toString().substring(0, sb.length() - 2);
        // Lambdas erst ab API 24
//        return authors.stream().map(a -> a.getLastName() + " " + a.getFirstName())
//                .collect(Collectors.joining(", "));
    }
}
