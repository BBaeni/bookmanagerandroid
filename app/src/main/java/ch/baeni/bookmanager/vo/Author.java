package ch.baeni.bookmanager.vo;

/**
 * Created by Benjamin on 30.06.2017.
 */

public class Author {
    private final String lastName;
    private final String firstName;

    public Author(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }
}
