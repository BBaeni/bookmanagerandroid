package ch.baeni.bookmanager.dao;

import java.util.List;

import ch.baeni.bookmanager.vo.Book;

/**
 * Created by Benjamin on 30.06.2017.
 */

public interface BookDAO {
    List<Book> getAllBooks();

    void updateBook(Book book);

    void deleteBook(Book book);

    void addBook(Book book);

    List<Book> getBooksByProperties(String title, String author, int publishedYear, String isbn);
}
