package ch.baeni.bookmanager.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Benjamin on 30.06.2017.
 */

class ConnectionFactory {
    private static ConnectionFactory instance = new ConnectionFactory();
    private static final String DB_URL = "jdbc:mysql://192.168.1.14:3306/books";
    private static final String USER = "TestUser";
    private static final String PASS = "TestUser";

    private ConnectionFactory() {
    }

    private Connection createConnection() {
        Connection connection = null;
        try {
            // Bei API 19 noetig, da sonst der Treiber nicht gefunden wird...
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection() {
        return instance.createConnection();
    }
}
